<?php

namespace App\Http\Controllers;
use App\Models\Contacto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactoController extends Controller
{
    //
    
    public static function index( ){
        
        $contactos = Contacto::all();
        
        return view('listado.listado' , compact('contactos'));
    }
    
    public static function eliminar(Request $request){
        $contacto = Contacto::findOrFail($request -> id);
        
        if($contacto -> delete()){
            return ['resultado' => true , 'msg' => 'El contacto ha sido eliminado'];
        }else{
            
            return ['resultado' => false , 'msg' => 'Ocurrio un error'];
            
            
        }
        
    }
    
    public static function guardar(Request $request ){
        $data =Validator::make($request->all(),[
            'nombre'	  => 'required' ,
            'calle'=> 'required',
            'numero'=> 'required|numeric',
            'colonia'	  => 'required' ,
            'ciudad' => 'required',
            'longitud' => 'required'   ,
            'latitud' => 'required',
            'email' => 'required|email',
            'codigopostal' => 'required|numeric',
            'estado' => 'required'
            
          
        ]);
       
        if($data -> fails()){
            return redirect()->route('contacto.listado') ->with('fails', $data ->errors());
       }
       
       if($request -> id){
        
            $contacto = Contacto::findOrFail($request->id);
            $contacto -> update($request->all());
            
            return redirect()->route('contacto.listado') -> with('success', 'Se modifico a ' . $request -> nombre);
            
        
        
       }else {
            Contacto::create($request->all());


            return redirect()->route('contacto.listado') -> with('success', 'Se agrego a ' . $request -> nombre);
        
       }
       
       

        
        
    }
    
    
}
