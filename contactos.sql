-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-06-2022 a las 04:40:08
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contactos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `calle` varchar(40) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `colonia` varchar(25) NOT NULL,
  `ciudad` varchar(25) NOT NULL,
  `codigopostal` varchar(8) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `longitud` varchar(150) NOT NULL,
  `latitud` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id`, `nombre`, `calle`, `numero`, `colonia`, `ciudad`, `codigopostal`, `estado`, `email`, `longitud`, `latitud`) VALUES
(1, 'Joel', 'Cordo 5542', '6671527017', 'Cedros', 'Culiacan', '', '', '', '24.817952', '-107.440026'),
(7, 'Joel1', 'Avenida Troyes', '6671527017', 'Montecarlo', 'Culiacán', '80054', 'Sinaloa', 'jgastelumro@gmail.com', '-107.4244094', '24.8151501');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
