 

@extends('welcome')
@section('content')
<div class="row ">
    @foreach($contactos  as $contacto )
        <div class="col-sm-4 mt-3" id="cardContacto_{{ $contacto -> id}}">
            <div class="card">
            <div class="card-header">
                  {{  $contacto -> nombre  }}  - {{  $contacto ->numero }}
            </div>
            <div class="card-body">
            <p class="card-text"> {{  $contacto ->email  }} </p>
                
                <p class="card-text"> {{  $contacto ->calle }} , {{  $contacto ->colonia }} , {{  $contacto ->ciudad }}  , {{  $contacto ->estado }}  , {{  $contacto ->codigopostal }}  </p>
                <p class="card-text"> {{  $contacto ->longitud }}  , {{  $contacto ->latitud }} </p>
                
                <div class="row">
                    <div class="col-3 mr-1"> <a href="#" class="btn btn-primary"    onclick="editar( {{$contacto}} ) ">Editar</a> </div>
                    <div class="col-3"><a href="#" class="btn btn-danger" onclick="eliminar({{ $contacto -> id }}  , ' {{ $contacto -> nombre }}')">Eliminar</a> </div>
                    
                </div>
            </div>
            </div>
        </div>
    
    @endforeach
</div>    

<script>
    function eliminar( id , nombre ){
        swal({
            title: "Estas seguro que deseas eliminar a " + nombre + " de tu agenda?",
            text: "Una vez eliminado no podras recuperar el contacto",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                
                if(eliminarAjax(id)){
                    $("#cardContacto_" + id).remove();
                    
                    swal(nombre + " ha sido eliminado de tu Agenda", {
                        icon: "success",
                    });
                    
                }else{
                       
                    swal("Ocurrio un error al eliminar a " + nombre +" de tu lista de contactos, comunicate con administración", {
                        icon: "error",
                    });
                    
                }
                
                
           
            } else {
                swal("No se han realizado cambios a tu agenda!");
            }
            });
    }
    
    
    function eliminarAjax(id ){
        tResultado = false;
        
        $.ajax({
        type: "POST",
        async:false,
        url: "{{ route('contacto.eliminar') }}",
        data: { 
            id : id ,_token: '{{csrf_token()}}' 
        } ,
        dataType:"json",
        success: function(respuesta) {
                
            tResultado =  respuesta.resultado;
            console.log(respuesta.resultado);
            
        },
        error: function() {
        }
        });
        
        console.log(tResultado);
        return tResultado;
    }
</script>
@endsection