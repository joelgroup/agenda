<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.1.1/css/fontawesome.min.css" integrity="sha384-zIaWifL2YFF1qaDiAo0JFgsmasocJ/rqu7LKYH8CoBEXqGbb9eO+Xi3s6fQhgFWM" crossorigin="anonymous">
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
        </style>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <header>
        <nav class="navbar navbar-dark bg-dark">
            <div class="col-12 text-center">
                <h2   > Agenda Télefonica </h2>
            </div>
        </nav>
    </header>
    <section class="container">
    @if (session('fails'))
        @php $errores = session('fails') @endphp
      
        <div class="alert alert-error">
            {{ $errores}}
        </div>
        
    @endif
    @if (session('success'))
        <div class="alert alert-success mt-4">
            {{session('success')}}
        </div>
    @endif
    @yield('content')
       
    
    </section>
    <a onclick="nuevo()" class="float_but"  >
        <i class="fa fa-plus my-float_bt">   </i>
    </a>
    
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modalNuevo">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('contacto.guardar') }}" id="formGuardar" method="POST">
                    <input type="text" style="display:none" name="id" id="id">
                    {{ csrf_field() }} 
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nombre">*Nombre:  </label>
                                    <input type="text" name="nombre" class="form-control" id="nombre" aria-describedby="nombreHelp" placeholder="Escribe el nombre...">
                                    <small id="nombreHelp" class="form-text text-muted"> </small>
                                </div>
                            </div>
            
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="numero">*Número:  </label>
                                    <input type="text"  name="numero"   class="form-control" id="numero" aria-describedby="numeroHelp" placeholder="Escribe el numero..." maxlength="10" onKeypress="if (event.keyCode >57) event.returnValue = false;">
                                    <small id="numeroHelp" class="form-text text-muted">.</small>
                                </div>
                            </div>  
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="email">*Correo Electronico:  </label>
                                    <input type="email"  name="email"   class="form-control" id="email" aria-describedby="email" placeholder="Escribe el email...">
                                    <small id="emailHelp" class="form-text text-muted">.</small>
                                </div>
                            </div>        
                        </div>
                        <div class="row">
                            <div class="col-12 text-center"> <h5> Datos del Domicilio </h5></div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="calle">*Calle:  </label>
                                    <input type="text" name="calle"  class="form-control" id="calle" aria-describedby="calleHelp" placeholder="Escribe la calle...">
                                    <small id="calleHelp" class="form-text text-muted"></small>
                                </div>
                            </div>    
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="colonia">*Colonia:  </label>
                                    <input type="text"   name="colonia"  class="form-control" id="colonia" aria-describedby="coloniaHelp" placeholder="Escribe la colonia...">
                                    <small id="coloniaHelp" class="form-text text-muted"></small>
                                </div>
                            </div>           
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="Ciudad">*Ciudad:  </label>
                                    <input type="text"  name="ciudad"  class="form-control" id="ciudad" aria-describedby="CiudadHelp" placeholder="Escribe la Ciudad...">
                                    <small id="CiudadHelp" class="form-text text-muted"></small>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="Ciudad">*Codigo Postal:  </label>
                                    <input type="text"  name="codigopostal"  class="form-control" id="codigopostal" aria-describedby="codigopostalHelp" placeholder="Escribe el Codigo postal..." onKeypress="if (event.keyCode >57) event.returnValue = false;">
                                    <small id="codigopostalHelp" class="form-text text-muted"></small>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="Ciudad">*Estado:  </label>
                                    <input type="text"  name="estado"  class="form-control" id="estado" aria-describedby="estadoHelp" placeholder="Escribe el estado...">
                                    <small id="estadoHelp" class="form-text text-muted"></small>
                                </div>
                            </div>
                            
                   
                        </div>
                        <div class="row">
                        <div class="col-12"> <button type="button" class="btn btn-info" onclick="cargaGPS()"> Cargar GPS </button>  </div>
                        <div class="col-6">
                                <div class="form-group">
                                    <label for="nombre">*Longitud:  </label>
                                    <input type="text"  name="longitud"  class="form-control" id="longitud" aria-describedby="longitudHelp" placeholder="Escribe la longitud...">
                                    <small id="longitudHelp" class="form-text text-muted"></small>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="latitud">*Latitud:  </label>
                                    <input type="text"  name="latitud"  class="form-control" id="latitud" aria-describedby="latitudHelp" placeholder="Escribe la latitud...">
                                    <small id="latitudHelp" class="form-text text-muted"></small>
                                </div>
                            </div>
                        
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="validaCampos()">Guardar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    
    
    
       
    </body>
    
    <script>
        function cargaGPS(){
            let url = "https://nominatim.openstreetmap.org/search?format=json&limit=1&q={"+ $('#calle').val() +"," + $('#colonia').val( ) + "," + $('#ciudad').val() + "," + $('#estado').val( ) + "," + $('#codigopostal').val() + "}";
            console.log(url);
            $.ajax({
                type: "GET",
                url: url,
                success: function(respuesta) {
                     console.log(respuesta);
                    if(respuesta.length > 0 ){
                        $('#longitud').val( respuesta[0].lon);
                        $('#latitud').val( respuesta[0].lat);
                     
                        
                    }else{
                        swal("No se encontro domicilio!");
                        
                    }
                 
                    
                    
                },
                error: function(error) {
                    console.log(error);
                }
            });
            
        }
        
        
        function nuevo(){
            $('#id').val() ;
            
            $('#nombre').val( '') ;
            $('#numero').val('') ;
            $('#email').val('' ) ;
            $('#calle').val( '') ;
            $('#colonia').val( '');
            $('#ciudad').val('');
            $('#longitud').val( '');
            $('#latitud').val( '');
            $('#estado').val( '');
            $('#codigopostal').val( '');
            
            $('#exampleModalLabel').text('Nuevo Contacto')
            
            $("#modalNuevo").modal('show');
        }
 
        
        function editar( contacto ){
            $('#id').val( contacto.id) ;
            
            $('#nombre').val( contacto.nombre) ;
            $('#numero').val( contacto.numero) ;
            $('#email').val( contacto.email) ;
            $('#calle').val( contacto.calle) ;
            $('#colonia').val( contacto.colonia);
            $('#ciudad').val(contacto.ciudad);
            $('#longitud').val( contacto.longitud);
            $('#latitud').val( contacto.latitud);
            $('#estado').val( contacto.estado);
            $('#codigopostal').val( contacto.codigopostal);
            $('#exampleModalLabel').text('Editar Contacto')
            
            $("#modalNuevo").modal('show');
        }
        
        function validaCampos(){
            
            
            if($('#nombre').val() == ""){
                $('#nombreHelp').text("No puede estar vacio.");
                return false;
                
            }else{
                
                $('#nombreHelp').text('');
                
            }
            if($('#numero').val() == ""){
                $('#numeroHelp').text("No puede estar vacio.");
                return false;
                
            }else{
                
                $('#numeroHelp').text('');
                
            }
            if($('#email').val() == ""){
                $('#emailHelp').text("No puede estar vacio.");
                return false;
                
            }else if( verificaMail($('#email').val() )) {
                $('#emailHelp').text("Email no valido.")
                return false;
            }else{
                
                $('#emailHelp').text('');
                
            }
            if($('#calle').val() == ""){
                $('#calleHelp').text("No puede estar vacio.");
                return false;
                
            }else{
                
                $('#calleHelp').text('');
                
            }
            if($('#colonia').val() == ""){
                $('#coloniaHelp').text("No puede estar vacio.");
                return false;
                
            }else{
                
                $('#coloniaHelp').text('');
                
            }
            if($('#ciudad').val() == "") {
                $('#CiudadHelp').text("No puede estar vacio.");
                return false;
                
            }else{
                
                $('#CiudadHelp').text('');
                
            }
            if($('#longitud').val() == ""){
                $('#longitudHelp').text("No puede estar vacio.");
                return false;
                
            }else{
                
                $('#longitudHelp').text('');
                
            }
            if($('#latitud').val() == ""){
                $('#latitudHelp').text("No puede estar vacio.");
                return false;
                
                
            }else{
                
                $('#latitudHelp').text('');
                
            }
            if($('#estado').val() == ""){
                $('#estadoHelp').text("No puede estar vacio.");
                return false;
                
                
            }else{
                
                $('#estadoHelp').text('');
                
            }          
            if($('#codigopostal').val() == ""){
                $('#codigopostal').text("No puede estar vacio.");
                return false;
                
                
            }else{
                
                $('#codigopostalHelp').text('');
                
            }
            
            $('#formGuardar').submit();
    }
    
    function verificaMail (valor){
        re=/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
        if(!re.exec(valor)){
            return true;
        }
        else return false;
    }
    
    
    </script>
    
    <style>
        h2{
            color:whitesmoke;
            margin:auto;
            
        }
        
        .float_but{
            position:fixed;
            bottom:80px;
            right:40px;
            width:60px;
            height:60px;
            background-color:#f60;
            color:#FFF;
            border-radius:50px;
            text-align:center;
            box-shadow: 2px 2px 3px #999;
        }

        .my-float_bt{
            margin-top:17px;
            font-size:30px;
        }
        
    </style>
    
  
</html>


